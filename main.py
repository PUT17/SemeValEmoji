from sklearn.linear_model import SGDClassifier

from TweetsPredictor import TweetsPredictor

if __name__ == '__main__':
    # Example of usage

    # Create instance
    our_tweets = TweetsPredictor(raw_file_text='us_data/us_train.text', raw_file_labels='us_data/us_train.labels',
                                 emojis_file='us_data/us_mapping.txt')
    # Load all Tweets
    our_tweets.load_tweet(None)
    # Load 100 000 tweets
    # our_tweets.load_tweet(1000000)

    # Prepare estimator to test
    list_of_estimators = [
        # MultinomialNB(),
        SGDClassifier(n_jobs=-1, loss='hinge', penalty='l2',
                      alpha=1e-3, random_state=42,
                      max_iter=5, tol=None),
        # RandomFore(stClassifier(n_jobs=-1),
        # ExtraTreesClassifier(n_jobs=-1),
        # GradientBoostingClassifier(),
        # RidgeClassifier(solver='sag'),
        # XGBClassifier(n_jobs=-1)
    ]
    # Prepare us_data to fit and predict
    x_train, x_test, y_train, y_test = our_tweets.divide_data()

    # Fitting estimators
    our_tweets.fit_estimators(list_of_estimators, x_train=x_train, y_train=y_train)

    # View summary
    our_tweets.summary(x_test=x_test, y_test=y_test)

    # View big summary
    our_tweets.big_summary(step_size=250000, step_ratio=0.5, list_of_estimators=list_of_estimators)

# Table of Contents
1. [Task Description](#semvalemoji)
2. [How to use](#how-to-use)
3. [Architectures](#architectures)

# SemValEmoji
Simple solution for prediction emoji in tweets, which was one of task in SemVal 20018 competition. 
[Lint to task](https://competitions.codalab.org/competitions/17344 "SemVal2018 Page")

Console program writted in Python 3.6.  
Used libraries:
- sklearn used for obtain classification, preparing data and cleaning data, e. stamping, lemmatization, stop words, 
- pandas used for store data, 
- nltk used for n grams
- matplotlib.pyplot used for some graphs

Solution was tested on english data set.

# How to use

1. Obtain repository with ```git clone https://gitlab.com/PUT17/SemeValEmoji.git```
2. (optional) Run ```pip install -c .\requirements.txt``` to install libraries
3. Run ```python main.py```

# Architectures
File ```main.py``` contain a simple case of usage ```TweetPredictor``` class with short descriptions each line.  
```HelpFunctions.py``` file - there are several helpful functions, which was used in project.  
The hearth of the program is in ```TweetsPredictor.py``` file. Contains a class with the same name. 
This class loading data, fitting estimator and viewing a summary of predictions.


All rights reserved.
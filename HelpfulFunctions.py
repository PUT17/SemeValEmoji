from collections import Counter

import matplotlib.pyplot as plt
import pandas as pd
from nltk import ngrams, OrderedDict
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import SGDClassifier


def get_chunks(iterable, chunk_size):
    """
    Generator to divide us_data and get chunk us_data to processing.
    :param iterable:
    :param chunk_size:
    :return:
    """
    size = len(iterable)
    if size < chunk_size:
        yield iterable
    chunks_nb = int(size / chunk_size)
    iter_ints = range(0, chunks_nb)
    for i in iter_ints:
        j = i * chunk_size
        if i + 1 < chunks_nb:
            k = j + chunk_size
            yield iterable[j:k]
        else:
            yield iterable[j:]


def get_n_grams(list_of_words):
    """
    Create 1-grams and 2-grams from list of words
    :param list_of_words:
    :return dictionary of words { 'word': 1}:
    """
    n_grams = dict()
    allterms = ' '.join(list_of_words).split(' ')
    # create 1-grams, 2-grams and 3-grams and zip() them all.
    for g in zip(ngrams(allterms, 1), ngrams(allterms, 2)):  # , n_grams(allterms, 3)):
        for w in map(lambda w: ' '.join(w), g):
            if w not in n_grams:  # and not search_stop_words(w):
                n_grams[w] = 1
    return n_grams


def process_vocabulary(texts):
    """
    Get n_grams of words
    :param texts:
    :return list of n grams:
    """
    ngrams = get_n_grams(texts)
    keys = ngrams.keys()
    # keys.sort()
    return keys


def testing_functions_parameters(x_test, y_test, tweets_predictor=None, list_of_estimators=None):
    """
    Testing parameters of each estimators and prepare functions e.q CountVectorizer()
    :param x_test:
    :param y_test:
    :param tweets_predictor:
    :param list_of_estimators:
    :return:
    """
    param_grid_rest = dict(
        # vect__analyzer=('word', 'char', 'char_wb'),
        # vect__lowercase=[True, False],
        # vect__ngram_range=[(1, 1), (1, 2), (1, 3)],
        vect__stop_words=('english', None),
        vect__lowercase=[True, False],
        # hash__ngram_range=[(1, 1), (1, 2)],
        # tfidf__use_idf=[True, False]
    )

    param_grid = dict(
        # MultinomialNB__fit_prior=[True, False],
        # SGDClassifier__loss=('hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron')
        # SGDClassifier__penalty=['l2', 'none', 'l1', 'elasticnet'],
        # SGDClassifier__alpha=[1e-3],
        # SGDClassifier__random_state=[42, 50],
        # SGDClassifier__max_iter=[4, 5, 6],
        # SGDClassifier__tol=[None],
        # RidgeClassifier__solver=['auto', 'cholesky'],  # , 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag', 'saga']
        RidgeClassifier__normalize=[True, False]
    )

    tweets_predictor.looking_for_best_parameters(param_grid, param_grid_rest, list_of_estimators,
                                                 x_train=x_test,
                                                 y_train=y_test)


def testing_n_grams_chunk():
    """
    Testing n-grams features and chunk function
    :return:
    """
    from scipy import sparse
    data = ['Happy birthday Paula! Sorry you hate this picture, but I look good @ Fizz Soda Bar',
            '@user The model for Edna Mode.',
            'Lunch date with mom @ Los Generales',
            'I will never leave my #brothers we side by side when it comes to becoming #successful #loyalty…',
            'The Who and Roger Waters (@ Desert Trip in Indio, Calif)',
            'My girl fernweh_jenny felt so good after her haircut, she posted this selfie. Stunning @user']

    vocabulary = process_vocabulary(data)
    model = CountVectorizer(
        ngram_range=(1, 3), analyzer='word',
        vocabulary=vocabulary
    )
    dtm_chunked = []
    for chunk in get_chunks(data, 2):
        dtm_chunked.append(model.fit_transform(chunk))

    dtm = sparse.vstack(dtm_chunked)
    estimator = SGDClassifier()
    estimator.fit(dtm)


def check_amount_of_emojis_types(series_labels):
    """
    Produce histogram of emojis.
    :param series_labels: Series type from pandas
    :return:
    """
    counter = Counter(series_labels)
    counter = OrderedDict(sorted(counter.items()))
    x = list(counter.keys())
    y = list(counter.values())
    fig, ax = plt.subplots()
    width = 0.9  # the width of the bars
    ind = pd.np.arange(len(y))  # the x locations for the groups
    ax.barh(ind, y, width, color="blue")
    ax.set_yticks(ind + width / 2)
    ax.set_yticklabels(x, minor=False)
    for i, v in enumerate(y):
        ax.text(v + 3, i + .25, str(v), color='blue', fontweight='bold')
    plt.title('Count each emoji')
    plt.xlabel('Amount')
    plt.ylabel('Emoji')
    plt.show()

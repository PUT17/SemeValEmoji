from os import linesep

import matplotlib.pyplot as plt
import pandas as pd
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.metrics import precision_score, recall_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.pipeline import Pipeline

from HelpfulFunctions import process_vocabulary


class TweetsPredictor:
    """
    Main class, storing tweets, fir estimators, get prediction result.
    :param str raw_file_text:
    :param str raw_file_labels:
    :param dict emojis:
    :param list prep_processing_function_list: list of tuples where ('name': pre-processing functions())
    :param str emojis_file:
    :param tuple range_n_grams: tuple with range eg (1, 2) means 1 grams and 2 grams default (1, 1)
    """

    def __init__(self, raw_file_text='us_trial.text', raw_file_labels='us_trial.labels', emojis=None,
                 prep_processing_function_list=None, emojis_file='us_mapping.txt', range_n_grams=(1, 1)):
        super()
        self.range_n_grams = range_n_grams
        self.vocabulary = 0
        self.data_size = 0
        self.data = 0
        self.raw_file_text = raw_file_text
        self.raw_file_labels = raw_file_labels
        self.emojis_file = emojis_file
        self.emojis_name = emojis
        if self.emojis_name is None:
            self.set_emoji()

        self.list_of_fitted_estimators = list()
        if prep_processing_function_list is not None:
            self.pre_processing_function_list = prep_processing_function_list
        self.set_prep_processing_function_list()

    def set_emoji(self):
        """
        Set self.emojis_name values (list) from self.emojis_file.
        :return:
        """
        self.emojis_name = list()
        with open(self.emojis_file, encoding='utf8') as emojis:
            self.emojis_name = [emoji.split('\t')[1] for emoji in emojis.read().splitlines()]

    def set_prep_processing_function_list(self):
        """
        Set default value of pre_processing_list to:
        [
            ('vect': CountVectorizer(ngram_range=(1,1))),
            ('tfidf': TfidfTransformer())
        ]
        :return:
        """
        self.pre_processing_function_list = [
            # ('hash', HashingVectorizer(ngram_range=(1, 2), non_negative=True)),
            ('vect', CountVectorizer(ngram_range=self.range_n_grams)),  # , vocabulary=self.vocabulary)),
            ('tfidf', TfidfTransformer()),
            # ('fpe', SelectFpr())
            # ('per', SelectPercentile())
            # ('fwe', SelectFwe())
            # ('select_best', SelectKBest(k=1000)),
        ]

    def load_tweet(self, slice_data_set=None):
        """
        Load tweets from self_raw_file_text and labels from self.file_labels and set inside variable (us_data, data_size)
        :param int slice_data_set: number us_data set size to load:
        :return:
        """
        data_text = pd.read_table(self.raw_file_text, index_col=False, names=['text'], sep=linesep, engine='python')
        data_label = pd.read_table(self.raw_file_labels, index_col=False, names=['label'], sep=linesep, engine='python')
        self.data = data_text.join(data_label)
        if slice_data_set is not None:
            self.data = self.data[:slice_data_set]
        self.data_size = len(self.data)

    def divide_data(self, percent_of_test_data=0.2, random_state=0):
        """
        Divide us_data on test_set and train_set by percent.
        :param float percent_of_test_data: range 0 - 1
        :param int random_state:
        :return X_train, X_test, y_train, y_test:
        """
        return train_test_split(self.data['text'], self.data['label'], test_size=percent_of_test_data,
                                random_state=random_state)

    def fit_estimators(self, list_of_estimator, x_train, y_train):
        """
        Fitting defined estimators.
        list of estimators functions e.q.  [RidgeClassifier(solver='sag'), ]
        :param list_of_estimator:
        :param x_train:
        :param y_train:
        :return:
        """
        self.list_of_fitted_estimators = list()
        for estimator in list_of_estimator:
            print('Fitting: {0}'.format(estimator.__class__.__name__))
            text_clf = Pipeline(self.pre_processing_function_list + [
                (estimator.__class__.__name__, estimator),
            ])
            text_clf.fit(x_train, y_train)
            self.list_of_fitted_estimators.append([estimator.__class__.__name__, text_clf])

    def predict(self, x_test):
        """
        Calculate prediction each estimators.
        :param x_test:
        :return result list of estimators predictions:
        """
        result = list()
        for estimator in self.list_of_fitted_estimators:
            result.append([estimator[0], estimator[1].predict(x_test)])
        return result

    def summary(self, x_test, y_test):
        """
        Print to console summary of prediction, show recall, precision, f1...
        :param x_test:
        :param y_test:
        :return:
        """
        for estimator in self.list_of_fitted_estimators:
            print('{0}: predicting...'.format(estimator[0]))
            predicted = estimator[1].predict(x_test)
            print('Estimator: {0}'.format(estimator[0]))
            print('Mean recall: {0:.3f}'.format(pd.np.mean(predicted == y_test)))
            # precision = precision_score(y_test, predicted, average=None)
            # recall = recall_score(y_test, predicted, average=None)
            print(metrics.classification_report(y_test, predicted, target_names=self.emojis_name))

    def looking_for_best_parameters(self, parameters_classifier_dict, parameters_dict_rest, list_of_estimator, x_train,
                                    y_train, cores=-1):
        """
        Function which help to establish parameters to estimators. Print result to console.
        :param parameters_classifier_dict:
        :param parameters_dict_rest:
        :param list_of_estimator:
        :param x_train:
        :param y_train:
        :param cores number of using cores :
        :return:
        """
        for estimator in list_of_estimator:
            param_grid = dict(
                [(x, y) for x, y in parameters_classifier_dict.items() if x.startswith(estimator.__class__.__name__)])
            if not param_grid:
                continue
            param_grid.update(parameters_dict_rest)
            print('Estimator: {0}'.format(estimator.__class__.__name__))
            pipeline = Pipeline(self.pre_processing_function_list + [
                (estimator.__class__.__name__, estimator),
            ])
            grid_search = GridSearchCV(pipeline, param_grid=param_grid, verbose=1, n_jobs=cores)
            grid_search.fit(x_train, y_train)
            print(grid_search.best_params_)

    def create_vocabulary(self):
        """
        Set vocabulary from us_data set.
        :return:
        """
        self.vocabulary = process_vocabulary(self.data['text'])

    def big_summary(self, step_size=50000, step_ratio=0.2, list_of_estimators=None):
        """
        More information about processing tweets. Iterate by us_data set size and ratio. Not ready yet.
        :param step_ratio:
        :param step_size:
        :param list_of_estimators:
        :return:
        """
        if not self.emojis_name:
            self.get_emoji()
        if not self.pre_processing_function_list:
            self.set_prep_processing_function_list()

        list_of_results = list()
        for data_size in range(step_size, self.data_size + step_size, step_size):
            self.load_tweet(data_size)
            print('Data set size: {0}'.format(data_size))
            for ratio in pd.np.arange(0.2, 1.0, step_ratio):
                print('Ratio: {0}'.format(ratio))
                x_train, x_test, y_train, y_test = self.divide_data(percent_of_test_data=ratio)
                self.fit_estimators(list_of_estimators, x_train=x_train, y_train=y_train)
                for estimator in self.list_of_fitted_estimators:
                    predicted = estimator[1].predict(x_test)
                    precision = precision_score(y_test, predicted, average=None)
                    recall = recall_score(y_test, predicted, average=None)
                    list_of_results.append([estimator[0], data_size, ratio, precision, recall])
        # todo define right plot view
        df = pd.DataFrame(list_of_results, columns=['Classificator', 'data_size', 'ratio', 'precision', 'recall'])
        print(df)
        df.plot.bar()
        plt.show()
        print()
